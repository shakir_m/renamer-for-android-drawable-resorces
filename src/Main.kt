import java.io.File

public class Main {


    companion object {

        var folderCount = 0
        val renamed_foldeers = arrayListOf<String>()
        var fileCount = 0
        var renamed_fileCount = 0


        @JvmStatic
        fun main(args: Array<String>) {

            println("choose platform : androidCopy, androidRename or flutter, userGuide ,android_Folders_EN_AR_Rename")
            val platform = readLine()


            if ("androidCopy".toLowerCase().startsWith(platform?.toLowerCase() ?: "_")) {
                androidCopy()
            } else if ("androidRename".toLowerCase().startsWith(platform?.toLowerCase() ?: "_")) {
                android()
            } else if ("userGuide".toLowerCase()
                    .startsWith(platform?.toLowerCase() ?: "_")
            ) {
                userGuide()
            } else if ("flutter".toLowerCase().startsWith(platform?.toLowerCase() ?: "_")) {
                flutter()
            } else if ("android_Folders_EN_AR_Rename".toLowerCase()
                    .startsWith(platform?.toLowerCase() ?: "_")
            ) {
                flutter()
            } else {
                main(args);
            }


        }


        fun getMainDir(s: String = "drag and drop directory path  OR COPY PASTE"): File {
            println(s)

            var path = readLine()
            if (path.isNullOrBlank()) {
                return getMainDir()
            }
            //mac android studio drag and drop
            path = path.replaceFirst("file:", "")
            path = path.trim()
            path = path.removeSurrounding("\"")
            path = path.removeSurrounding("\'")
            //mac drag and drop space issue
            path = path.replace("\\ ", " ")


            val mainDir = File(path)
            println()
            if (!mainDir.exists()) {
                println("mainDir NOT EXIST ===================")
                return getMainDir()
            }

            return mainDir
        }


        fun flutter() {
            val mainDir = getMainDir()
            mainDir?.listFiles()?.forEach {
                renameFileForFlutter(it)
            }
            println()
            println("Files:$fileCount Renamed:$renamed_fileCount")
            println("DIRS:$folderCount Renamed:${renamed_foldeers.size}")
            println("main completed")
        }


        fun android() {
            val mainDir = getMainDir()
            mainDir?.listFiles()?.forEach {
                renameFileAndroid(it)
            }
            println()
            println("Files:$fileCount Renamed:$renamed_fileCount")
            println("DIRS:$folderCount Renamed:${renamed_foldeers.size}")
            copyToAndroidProjcet(mainDir)
            println("main completed")
        }


        fun userGuide() {
            val mainDir = getMainDir()
            mainDir?.listFiles()?.forEach {
                if (it.isDirectory)
                    it.listFiles().forEach {
                        it.renameTo(
                            File(
                                it.parentFile,
                                it.parentFile.parentFile.name + "_" + it.parentFile.name.replace(
                                    "-AR",
                                    "",
                                    true
                                ).replace("-EN", "", true)
                                    .plus("_" + it.name).toLowerCase().replace("&", "_")
                                    .replace("-", "_")
                            )
                        )
                    }
            }
            ///Users/balachandran/Documents/ug
            val ar = File("ar").apply { mkdir() }
            val en = File("en").apply { mkdir() }
            mainDir.listFiles().forEach {
                if (it.isDirectory) {
                    it.listFiles().forEach {
                        if (it.parentFile.name.endsWith("ar", true))
                            it.copyTo(File(ar, it.name))
                        else
                            it.copyTo(File(en, it.name))
                    }
                }
            }


            println()
            println("Files:$fileCount Renamed:$renamed_fileCount")
            println("DIRS:$folderCount Renamed:${renamed_foldeers.size}")

        }


        fun androidCopy() {
            val mainDir = getMainDir()
            val files = mainDir?.listFiles()
            val resDir = getMainDir("drag and drop res directory path  OR COPY PASTE")
            println("copy paste with rename STARTING $mainDir\n$resDir")

            val dlist = arrayListOf<File>()
            fun getDrawableDirs(files: Array<File>?) {
                files?.forEach {
                    if (it.isDirectory && drawaleFolders.contains(it.name)) {
                        dlist.add(it)
                    } else if (it.isDirectory) {
                        getDrawableDirs(it.listFiles())
                    }

                }
            }
            getDrawableDirs(files)
            dlist.forEach {
                val resDrawDir = File(resDir, it.name)
                if (!resDrawDir.exists())
                    resDrawDir.mkdirs()
                it.listFiles().forEach {
                    if (File(resDrawDir, it.name).exists()) {
                        for (c in 'a'..'z') {
                            val ext = "." + it.name.split(".").last()
                            val dest = File(resDrawDir, it.name.replace(ext, "") + "_$c" + ext)
                            if (!dest.exists()) {
                                it.copyTo(dest)
                                break
                            }

                        }
                    } else {
                        it.copyTo(File(resDrawDir, it.name))
                    }
                }
            }


        }


        fun renameFileAndroid(it: File) {
            if (it.isDirectory) {
                folderCount++
                it.listFiles().forEach {
                    renameFileAndroid(it)
                }
            } else {

                fileCount++
                if (
                    it.extension.equals("png", true) ||
                    it.extension.equals("jpg", true) ||
                    it.extension.equals("jpeg", true)
                ) {


                    var nameOnly = it.nameWithoutExtension.toLowerCase()
                    var newName = ""
                    nameOnly.forEachIndexed { index, c ->
                        if (c.toInt() in 'a'.toInt()..'z'.toInt() || c.toInt() in '0'.toInt()..'9'.toInt()) {
                            newName += c
                        } else {
                            newName += '_'
                        }
                    }


                    val fff = it.parentFile.parentFile.name.toLowerCase()
                        .replace("0", "")
                        .replace("1", "")
                        .replace("2", "")
                        .replace("3", "")
                        .replace("4", "")
                        .replace("5", "")
                        .replace("6", "")
                        .replace("7", "")
                        .replace("8", "")
                        .replace("9", "")
                        .replace("_", "")
                        .replace("-", "_")
                        .replace("&", "_")
                        .replace("/", "_")
                        .replace(":", "_")
                        .replace(" ", "_")
                        .replace("__", "_")

                    /*   fff.forEachIndexed { index, c ->
                           if (c.toInt() in 'a'.toInt()..'z'.toInt() || c.toInt() in '0'.toInt()..'9'.toInt()) {
                               folder_nameNew += c
                           } else {
                               folder_nameNew += '_'
                           }
                       }
   */



                    newName = fff + "_" + newName + "." + it.extension.toLowerCase()
                    newName = newName
                        .replace("_______", "_")
                        .replace("______", "_")
                        .replace("_____", "_")
                        .replace("____", "_")
                        .replace("___", "_")
                        .replace("__", "_")
                        .toLowerCase()


                    if (newName[0].toString().toIntOrNull() != null) {
                        newName = "image_$newName"
                    }
                    if (newName.startsWith("_"))
                        newName = newName.replaceFirst("_", "")

                    if (it.name != newName) {

                        for (z in 1..10000) {
                            val newFile = File(it.parent, newName)
                            if (newFile.exists()) {
                                newName =
                                    newFile.nameWithoutExtension + "_" + z + "." + newFile.extension
                            } else {
                                break
                            }
                        }


                        println()
                        println()
                        println("${it.name}:::$newName ${it.parent}")
                        renamed_fileCount++
                        it.renameTo(
                            File(
                                it.parent, newName
                            )
                        )

                        if (!renamed_foldeers.contains(it.parent))
                            renamed_foldeers.add(it.parent)

                    }

                }


            }
        }


        /*       fun getNextNumberedFileIfAlreadyExist(dir: File, name: String): String {
                   val newFile = File(dir.parent, name)
                   if (!newFile.exists()) {
                       return newFile.name
                   } else {
                       for (z in 1..Int.MAX_VALUE) {
                           if (newFile.nameWithoutExtension.last().isDigit())
                           val newFile = File(dir.parent, name.replace())
                           if (newFile.exists()) {
                               newFile.nameWithoutExtension + "_" + z + "." + newFile.extension
                           } else {
                               return newFile.name
                           }
                       }
                   }

                   throw NoSuchFileException(dir)
               }*/

        fun copyToAndroidProjcet(mainDir: File) {
            println("Copy images to  to android project specific drawable-dpi folders: yes or  no")
            val yesOrNo = readLine()
            if ("yes".toLowerCase().startsWith(yesOrNo?.toLowerCase() ?: "_")) {
                val res = getResDir()
                mainDir.listFiles().forEach {
                    copyToAndroidStudioProjcetRecursive(it, res)
                }

            } else {

            }
        }

        val drawaleFolders = arrayOf(
            "drawable-ldpi",
            "drawable-mdpi",
            "drawable-hdpi",
            "drawable-xhdpi",
            "drawable-xxhdpi",
            "drawable-xxxhdpi",
            "drawable-xxxxhdpi"/*NOT EXIST*/
        )

        fun copyToAndroidStudioProjcetRecursive(it: File, res: File) {
            if (it.isDirectory) {
                it.listFiles().forEach {
                    copyToAndroidStudioProjcetRecursive(it, res)
                }
            } else if (it.isFile) {
                if (
                    it.extension.equals("png", true) ||
                    it.extension.equals("jpg", true) ||
                    it.extension.equals("jpeg", true)
                ) {
                    if (drawaleFolders.contains(File(it.parent).name)) {
                        if (File(File(res, File(it.parent).name), it.name).exists()) {

                        } else {
                            it.copyTo(File(File(res, File(it.parent).name), it.name))
                        }

                    } else {

                    }
                }
            }

        }


        fun getResDir(): File {
            println("drag and drop or copy paste android project resource/res directory path or app module path")
            var path = readLine()
            if (path.isNullOrBlank()) {
                return getResDir()
            }
            //mac android studio drag and drop
            path = path.replaceFirst("file:", "")
            path = path.trim()
            path = path.removeSurrounding("\"")
            path = path.removeSurrounding("\'")
            //mac drag and drop space issue
            path = path.replace("\\ ", " ")


            val mainDir = File(path)
            println()
            if (!mainDir.exists()) {
                println("given  path NOT EXIST ===================")
                return getResDir()
            }
            if (mainDir.name == "res") {
                return mainDir
            } else if (mainDir.name == "app") {
                val res = File(mainDir, "src/main/res")
                if (res.exists()) {
                    return res
                }
            }
            println("given  path NOT EXIST ===================")
            return getResDir()
        }


        fun renameFileForFlutter(it: File) {

            if (it.isDirectory) {
                folderCount++
                it.listFiles().forEach {
                    renameFileForFlutter(it)
                }
            } else {

                fileCount++
                if (
                    it.extension.equals("gif", true) ||
                    it.extension.equals("png", true) ||
                    it.extension.equals("jpg", true) ||
                    it.extension.equals("jpeg", true)
                ) {

                    val xDimensions = arrayOf(
                        "1x",
                        "1.5x",
                        "2x",
                        "2.5x",
                        "3x",
                        "3.5x",
                        "4x"
                    )

                    xDimensions.forEach { xDimensions ->

                        if (it.nameWithoutExtension.trim().endsWith("@$xDimensions", true)) {
                            val dX = File(it.parent, "$xDimensions")
                            if (!dX.exists()) dX.mkdir()
                            val oldName = it.name
                            if (!renamed_foldeers.contains(it.parent))
                                renamed_foldeers.add(it.parent)
                            val fX = File(dX, it.name.replace("@$xDimensions", "", true))
                            it.renameTo(fX)
                            println()
                            println()
                            println("$oldName moved to $xDimensions->${fX.name}")
                            renamed_fileCount++


                        }

                    }


                }


            }
        }


    }
}